package com.reksa.karang.udemybraintrainer;

import android.os.CountDownTimer;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.Random;

public class MainActivity extends AppCompatActivity {

    Button btnStart, btn1, btn2, btn3, btn4, btnPlayAgain;
    TextView tvSum, tvResult, tvPoint, tvTimer;
    ArrayList<Integer> answers = new ArrayList<>();
    int locationOfCorrectAnswer;
    int score = 0;
    int numberOfQuestion = 0;
    RelativeLayout gameRelativeLayout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        btn1 = findViewById(R.id.btn_choose1);
        btn2 = findViewById(R.id.btn_choose2);
        btn3 = findViewById(R.id.btn_choose3);
        btn4 = findViewById(R.id.btn_choose4);
        btnPlayAgain = findViewById(R.id.btn_playAgain);

        btnStart = findViewById(R.id.btn_start);
        tvResult = findViewById(R.id.tv_result);
        tvPoint = findViewById(R.id.tv_point);
        tvSum = findViewById(R.id.tv_sum);
        tvTimer = findViewById(R.id.tv_timer);

        gameRelativeLayout = findViewById(R.id.gameRelativeLayout);

        btnStart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                btnStart.setVisibility(View.INVISIBLE);
                gameRelativeLayout.setVisibility(RelativeLayout.VISIBLE);

                playAgain(findViewById(R.id.btn_playAgain));
            }
        });
    }

    public void playAgain(View view) {
        score = 0;
        numberOfQuestion = 0;

        tvTimer.setText("30s");
        tvPoint.setText("0/0");
        tvResult.setText("");
        btnPlayAgain.setVisibility(View.INVISIBLE);

        generateQuestion();

        new CountDownTimer(30100, 1000) {
            @Override
            public void onTick(long millisUntilFinished) {
                tvTimer.setText(String.valueOf(millisUntilFinished / 1000) + "s");
            }

            @Override
            public void onFinish() {
                btnPlayAgain.setVisibility(View.VISIBLE);
                tvTimer.setText("0s");
                tvResult.setText("Your score: " + Integer.toString(score) + "/" + Integer.toString(numberOfQuestion));
            }
        }.start();
    }

    public void chooseAnswer(View view) {
        if (view.getTag().toString().equals(Integer.toString(locationOfCorrectAnswer))) {
            score++;
            tvResult.setText("Correct!");
        } else {
            tvResult.setText("Wrong!");
        }
        numberOfQuestion++;
        tvPoint.setText(Integer.toString(score) + "/" + Integer.toString(numberOfQuestion));

        generateQuestion();
    }

    private void generateQuestion() {
        Random random = new Random();
        int a = random.nextInt(21); //0 - 21
        int b = random.nextInt(21);


        tvSum.setText(Integer.toString(a) + " + " + Integer.toString(b));

        locationOfCorrectAnswer = random.nextInt(4);

        answers.clear();

        int incorrectAnswer;
        for (int i = 0; i < 4; i++) {
            if (i == locationOfCorrectAnswer) {
                answers.add(a + b);
            } else {
                incorrectAnswer = random.nextInt(41);

                while (incorrectAnswer == a + b) {
                    incorrectAnswer = random.nextInt(41);
                }

                answers.add(incorrectAnswer);
            }
        }

        btn1.setText(Integer.toString(answers.get(0)));
        btn2.setText(Integer.toString(answers.get(1)));
        btn3.setText(Integer.toString(answers.get(2)));
        btn4.setText(Integer.toString(answers.get(3)));
    }
}
